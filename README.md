# mantecchi

Turned man pages into Texinfo. It never got close to anything useable.
See a [blog article](https://onkobutanaake.de/it/formatting-man-is-hard.html)
for the long version. To sum it up there is no stylesheet or structured
approach to write a man page. Instead it is outlining format.

Man pages or Groff/ Troff only outline styles without any context. There
is no rule how a command or its arguments have to be written. In addition
parsers or renderers are quite relaxed and forgiving. Over the decades
things evolved and something like markup appeared. And it is very hard
to link into a man page that was transformed to HTML.

I had a lot of sparetime and instead of jaywalking train tracks and
disturbing German schedule a tool was born. It reads plain man pages and
puts out Texinfo.

This in turn can and should be edited further and it will not suite all
corner cases. But the average man page following Ubuntu- or Debian
guidelines yields a usable pre stage.

# Texinfo Sample

If you want to have a look at real Texinfo move on to the source code
of `tar` and find its `tar.texi` - file.

# FAQ

Why not Markdown? It suffers from the same principle: outline format
instead of providing context.

Which alternatives are out there?

* Docbook – can be created from Texinfo
* HTML – still very likely to get wrong
* reStructured Text – Python
* Source code – software should be easy to handle, self contained and self-explaining

# Dedication

Per i Capuleti e i Montecchi di Vincenzo Bellini.
