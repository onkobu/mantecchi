<?xml version="1.0" encoding="UTF-8"?>

<!--
	Usage based on a here document. Include this in your
	per-language stylesheet. Define templates named preamble and
	tail and that's it. 
--> 
<xsl:stylesheet version="2.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:exsl="http://exslt.org/common"
	xmlns:str="http://exslt.org/strings"
	xmlns:func="http://exslt.org/functions"
	extension-element-prefixes="exsl func">
<xsl:output method="text" />
<xsl:include href="common-templates.xsl" />

<xsl:variable name="uunderline" select="'====================================================='" />
<xsl:variable name="underline" select="'-----------------------------------------------------'" />

<xsl:template match="doc">
	<xsl:call-template name="preamble" />
<xsl:value-of select="@name" /> v<xsl:value-of select="@version" /><xsl:text>
</xsl:text>
<xsl:value-of select="substring($uunderline,0,string-length(@name)+string-length(@version)+3)" /><xsl:text>

</xsl:text>
<xsl:apply-templates select="description" /><xsl:text>
</xsl:text>
Usage:
<xsl:choose>
	<xsl:when test="usage"><xsl:value-of select="str:replace(usage/text(),'\', '\\')" /></xsl:when>
	<xsl:otherwise>${_ME}</xsl:otherwise>
</xsl:choose>
<xsl:text>
</xsl:text>
Options
<!-- options are also supported in modes -->
<xsl:apply-templates select="options/option" mode="general"/>

<xsl:choose>
	<xsl:when test="modes">
		<xsl:apply-templates select="modes/mode" /><xsl:text>
</xsl:text>
	</xsl:when>
</xsl:choose>
<xsl:call-template name="tail" />
	</xsl:template>

	<xsl:template match="mode">
Mode <xsl:value-of select="@name" /><xsl:text>
</xsl:text>
<xsl:value-of select="substring($underline,0,string-length(@name)+6)" /><xsl:text>
</xsl:text>
<xsl:value-of select="help/text()" /><xsl:text>
</xsl:text>
	</xsl:template>
	
	<xsl:template match="option" mode="general">
		<xsl:choose>
			<xsl:when test="@name = 'help' and not(descendant::*)">			
--help/ -h – Displays usage information.
			</xsl:when>
			<xsl:when test="@name = 'verbose' and not(descendant::*)">
--verbose/ -v – Enable detailed messages about activity.
			</xsl:when>
			<xsl:when test="@name and @short">
--<xsl:value-of select="@name" />/ -<xsl:value-of select="@short" /> 
<xsl:if test="@mode-option">
<xsl:call-template name="render-mode-values" />
</xsl:if><xsl:value-of select="text()" />
			</xsl:when>
		</xsl:choose>
	</xsl:template>

</xsl:stylesheet>
