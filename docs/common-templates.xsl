<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:exsl="http://exslt.org/common"
	xmlns:func="http://exslt.org/functions"
	extension-element-prefixes="exsl func">
	<xsl:template name="render-mode-values">
		<xsl:text> </xsl:text>&lt;<xsl:for-each select="/doc/modes/mode" >
			<xsl:if test="position() > 1"> | </xsl:if>
			<xsl:value-of select="@name" />
		</xsl:for-each>&gt;
	</xsl:template>
</xsl:stylesheet>
