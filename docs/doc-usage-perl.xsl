<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:exsl="http://exslt.org/common"
	xmlns:str="http://exslt.org/strings"
	xmlns:func="http://exslt.org/functions"
	extension-element-prefixes="exsl func">
<xsl:output method="text" />
<xsl:include href="doc-usage-heredoc.xsl" />

<xsl:template name="preamble">
#!/usr/bin/env perl
use strict;
use warnings;
# use Getopt::Long;

sub print_usage {
	print &lt;&lt;HEREDOC<xsl:text>
</xsl:text>
	</xsl:template>

	<xsl:template name="tail">
HEREDOC
}

	print_usage();
	</xsl:template>
</xsl:stylesheet>
