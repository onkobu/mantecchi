<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:exsl="http://exslt.org/common"
	xmlns:func="http://exslt.org/functions"
	extension-element-prefixes="exsl func">
<xsl:output method="text" />
<xsl:include href="common-templates.xsl" />
<xsl:template match="doc">
# <xsl:value-of select="@name" /><xsl:text> </xsl:text><xsl:value-of select="@version" /><xsl:text>

</xsl:text>
<xsl:apply-templates select="description" /><xsl:text>

</xsl:text>
<xsl:if test="usage">
Usage:

```
<xsl:value-of select="usage/text()" />
```
</xsl:if>
# General Options
<!-- options are also supported in modes -->
<xsl:apply-templates select="options/option" mode="general"/>

<xsl:choose>
	<xsl:when test="modes">
# Modes
		<xsl:apply-templates select="modes/mode" /><xsl:text>
</xsl:text>
	</xsl:when>
</xsl:choose>
# Author(s)<xsl:text>
</xsl:text>
<xsl:apply-templates select="authors/author" /><xsl:text>
</xsl:text>
	</xsl:template>

	<xsl:template match="author">
* "<xsl:value-of select="@name" />" &lt;<xsl:value-of select="@mail" />&gt;
	</xsl:template>

	<xsl:template match="option" mode="general">
		<xsl:choose>
			<xsl:when test="@name = 'help' and not(descendant::*)">			
--help/ -h – Displays usage information.
			</xsl:when>
			<xsl:when test="@name = 'verbose' and not(descendant::*)">
--verbose/ -v – Enable detailed messages about activity.
			</xsl:when>
			<xsl:when test="@name and @short">
--<xsl:value-of select="@name" />/ -<xsl:value-of select="@short" /> 
<xsl:if test="@mode-option">
<xsl:call-template name="render-mode-values" />
</xsl:if><xsl:value-of select="text()" />
			</xsl:when>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="mode">
## <xsl:value-of select="@name" /><xsl:text>

</xsl:text>
<xsl:value-of select="help/text()" /><xsl:text>
</xsl:text>
<xsl:if test="example">
```
<xsl:value-of select="/doc/@name" /><xsl:text> </xsl:text> <xsl:value-of select="example/text()" />
```
</xsl:if>
	</xsl:template>

</xsl:stylesheet>
